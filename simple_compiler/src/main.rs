///This file heavily uses both the wasmtime getting started example and the pest (parser) guide book

extern crate pest;
#[macro_use]
extern crate pest_derive;

use pest::iterators::Pair;
use pest::Parser;
use std::fmt::Write;
use std::fs;
use wasmtime::*;

#[derive(Parser)]
#[grammar = "ident.pest"]
struct IdentParser;

///A simple compiler that parses a file using Pest and produces wasm.
fn main() {
    let code_input = fs::read_to_string("hello.simpleLang").unwrap() + "###END OF FILE###";
    let pairs = IdentParser::parse(Rule::program, &code_input).unwrap_or_else(|e| panic!("{}", e)); //Pest parser guide book

    let mut funcs: Vec<Function> = Vec::new();
    for pair in pairs {
        funcs.push(gen_fun_def(pair));
    }
    println!("Input code: \n\n{}\n", code_input);
    println!("As expressions: {:?}\n", funcs);
    let instr = parse_funcs_to_wasm(funcs);
    println!("As Wasm instructions: {:?}\n", instr);
    let wat = wasm_funcs(instr);
    println!("Generated wasm: \n\n{}\n", wat);
    println!("Running wasm...\n");

    match wasm_execute(wat) {
        Ok(_o) => (),
        Err(e) => println!(
            "An error occurred while trying to parse and execute the given wasm: {}",
            e
        ),
    }
}
///A parse function expression
#[derive(Debug)]
struct Function(String, Expr);
////A simple data structure to hold expr for AST.
#[derive(Debug)]
enum Expr {
    Function(String, Box<Expr>),
    Number(String),
    Add(Box<Expr>, Box<Expr>),
    Var(String),
    SetDestSource(Box<Expr>, Box<Expr>), //I hate that I am using a box here for the dest, but it would be annoying to make var a seperate type
    Block(Vec<Expr>),
    Undefined,
}
///Generates AST for function definitions.
fn gen_fun_def(pairs: Pair<Rule>) -> Function {
    let mut name: String = String::from("UnspecifiedFunc");
    let mut body: Expr = Expr::Undefined;
    for inner_pair in pairs.into_inner() {
        match inner_pair.as_rule() {
            Rule::expr => body = gen_expr(inner_pair),
            Rule::identifier => name = String::from(inner_pair.as_str()),
            _ => {
                println!("{:?}", inner_pair);
                panic!()
            }
        };
    }
    Function(name, body)
}
///Generates AST for expressions.
fn gen_expr(pair: Pair<Rule>) -> Expr {
    //Used pest guide book and brute force
    match pair.as_rule() {
        Rule::expr => {
            let mut iter = pair.into_inner();
            let mut result = gen_expr(iter.next().unwrap());
            for val in iter {
                result = Expr::Add(Box::new(result), Box::new(gen_expr(val)));
            }
            result
        }
        Rule::function_call => gen_func(pair),
        Rule::num_const => Expr::Number(String::from(pair.as_str())),
        Rule::set_op => gen_set(pair),
        Rule::identifier => Expr::Var(pair.as_str().to_string()),
        Rule::block => gen_block(pair),
        _ => Expr::Undefined,
    }
}
///Generates AST for blocks.
fn gen_block(pairs: Pair<Rule>) -> Expr {
    let mut instructions = Vec::new();
    for inner_pair in pairs.into_inner() {
        instructions.push(gen_expr(inner_pair));
    }
    Expr::Block(instructions)
}
///Generates AST for assignement operations.
fn gen_set(pairs: Pair<Rule>) -> Expr {
    let mut identifier: String = String::from("UnspecifiedFunc");
    let mut value: Expr = Expr::Undefined;
    for inner_pair in pairs.into_inner() {
        match inner_pair.as_rule() {
            Rule::expr => value = gen_expr(inner_pair),
            Rule::identifier => identifier = String::from(inner_pair.as_str()),
            _ => panic!("{:?}", inner_pair),
        };
    }
    Expr::SetDestSource(Box::new(Expr::Var(identifier)), Box::new(value))
}
///Generates AST for generating functions. (only suppports single parameter).
fn gen_func(pairs: Pair<Rule>) -> Expr {
    //Used pest guide book and brute force
    let mut name: String = String::from("UnspecifiedFunc");
    let mut args: Expr = Expr::Undefined;
    for inner_pair in pairs.into_inner() {
        match inner_pair.as_rule() {
            Rule::expr => args = gen_expr(inner_pair),
            Rule::identifier => name = String::from(inner_pair.as_str()),
            _ => (),
        };
    }
    Expr::Function(name, Box::new(args))
}
///Stores the Wasm AST for each defined function.
#[derive(Debug)]
struct WasmFunction(String, Vec<Wasm>);

////An AST containing wasm instructions.
#[derive(Debug)]
enum Wasm {
    I32Const(String),
    Call(String),
    DeclareVar(String),
    SetVar(String),
    GetVar(String),
}
///Generates an AST of wasm instructions from my languages AST.
fn parse_funcs_to_wasm(funcs: Vec<Function>) -> Vec<WasmFunction> {
    let mut functions = Vec::new();
    for Function(name, body) in funcs {
        functions.push(WasmFunction(name, parse_expr_to_wasm(vec![body])))
    }
    functions
}
///Generates wasm instructions for single expression.
fn parse_expr_to_wasm(exprs: Vec<Expr>) -> Vec<Wasm> {
    let mut instr: Vec<Wasm> = Vec::new();
    for expr in exprs {
        match expr {
            Expr::Function(name, func_expr) => {
                instr.append(&mut parse_expr_to_wasm(vec![*func_expr]));
                instr.push(Wasm::Call(name));
            }
            Expr::SetDestSource(identifier, value) => {
                instr.append(&mut parse_expr_to_wasm(vec![*value]));
                if let Expr::Var(name) = *identifier {
                    instr.push(Wasm::DeclareVar(name.clone()));
                    instr.push(Wasm::SetVar(name));
                } else {
                    panic!("Expected var identifier got something else???");
                }
            }
            Expr::Var(name) => instr.push(Wasm::GetVar(name)),
            Expr::Number(numb) => instr.push(Wasm::I32Const(numb)),
            Expr::Add(l, r) => {
                instr.append(&mut parse_expr_to_wasm(vec![*l]));
                instr.append(&mut parse_expr_to_wasm(vec![*r]));
                instr.push(Wasm::Call("add".to_string()));
            }
            Expr::Block(instructions) => instr.append(&mut parse_expr_to_wasm(instructions)),
            Expr::Undefined => (),
        }
    }
    instr
}
///Generates source code for wasm from wasm AST.
fn wasm_funcs(funcs: Vec<WasmFunction>) -> String {
    let mut body: String = String::from(
        //https://crates.io/crates/wasmtime
        "(module\n\
        \t(import \"host\" \"print\" (func $print (param i32)))
        \t(import \"host\" \"add\" (func $add (param i32 i32) (result i32)))\n",
    );
    for WasmFunction(name, instr) in funcs {
        body += &wasm_func_to_string(name, instr);
    }
    body += "\n\t(export \"main\" (func $main))\n)";
    body
}
///Generates source code for a single function
fn wasm_func_to_string(name: String, instrs: Vec<Wasm>) -> String {
    let mut header: String = "\t(func $".to_string() + &name + " \n";
    let mut internal = String::new();
    //https://developer.mozilla.org/en-US/docs/WebAssembly/Reference/Numeric
    for instr in instrs {
        match instr {
            Wasm::I32Const(numb) => writeln!(&mut internal, "\t\ti32.const {}", numb).unwrap(),
            Wasm::Call(name) => writeln!(&mut internal, "\t\tcall ${}", name).unwrap(),
            Wasm::DeclareVar(name) => writeln!(&mut header, "\t\t(local ${} i32)", name).unwrap(),
            Wasm::GetVar(name) => writeln!(&mut internal, "\t\tlocal.get ${}", name).unwrap(),
            Wasm::SetVar(name) => writeln!(&mut internal, "\t\tlocal.set ${}", name).unwrap(),
        }
    }
    header + &internal + "\t)\n"
}
///Executes a string as wasm assembly
fn wasm_execute(wat: String) -> wasmtime::Result<()> {
    //function mostly copied from WASMTIME getting started: https://crates.io/crates/wasmtime
    let engine = Engine::default();
    let module = Module::new(&engine, wat)?;
    let mut store = Store::new(&engine, ());
    let print = Func::wrap(&mut store, |_caller: Caller<'_, ()>, param: u32| {
        println!("{}", param);
    });
    let add = Func::wrap(
        &mut store,
        |_caller: Caller<'_, ()>, a: u32, b: u32| -> u32 { a + b },
    );
    let instance = Instance::new(&mut store, &module, &[print.into(), add.into()])?;
    let main = instance.get_typed_func::<(), ()>(&mut store, "main")?;
    main.call(&mut store, ())?;
    Ok(())
}
