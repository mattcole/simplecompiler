use std::{any::Any, rc::Rc};
///The wrapper I designed for my language. It uses an Rc container in order to ideally/eventually perform clone on write.
#[derive(Clone)]
pub struct MyLangWrapper<T: Clone + ToOwned>(Rc<T>);
///This trait allows us to refer to the wrapper dynamically.
pub trait DynamicTrait {
    fn dynamic_clone(&self) -> Box<dyn DynamicTrait>;
    fn get_mut_internal(&mut self) -> &mut dyn Any;
}
///A basic function to allow us to apply the wrapper.
pub fn my_lang_wrap<T: Clone>(input: T) -> MyLangWrapper<T> {
    MyLangWrapper(Rc::new(input))
}
///This allows us to perform cloning dynamically using a vtable so that we can clone without extracting the type
impl<T: 'static + Clone + ToOwned> DynamicTrait for MyLangWrapper<T> {
    fn dynamic_clone(&self) -> Box<dyn DynamicTrait> {
        let self_as_owned = Self(self.0.to_owned());
        Box::new(self_as_owned)
    }
    fn get_mut_internal(&mut self) -> &mut dyn Any {
        Rc::make_mut(&mut self.0) as &mut dyn Any
    }
}
///A simple wrapper to hold our dynamic type. We have to use an additional box to hold the vtable.
struct DynamicClone(Box<dyn DynamicTrait>);
impl Clone for DynamicClone {
    fn clone(&self) -> Self {
        DynamicClone(self.0.dynamic_clone())
    }
}
///A dynamic wrapper that allows us to both dynamically refer to types as well as provides access to easy cloning.
#[derive(Clone)]
pub struct Dynamic(DynamicClone);

impl Dynamic {
    pub fn get_mut<T: 'static + Clone>(&mut self) -> &mut T {
        self.0 .0.get_mut_internal().downcast_mut().unwrap() //Just panic! Not great but works for this language
    }
    pub fn get<T: 'static + Clone>(&mut self) -> T {
        self.get_mut::<T>().clone()
    }
}
///Started writing a CustomFrom function. This ended up being way way more difficult than I thought and became a massive time sink. Implementing broad generalized implementations is pretty difficult. Additionally, I need some way to perform into at runtime when downcasting. However, that seems to require comprehensive explicit functions for every conversion which was just not feasible for this project.
trait CustomFrom<T> {
    fn custom_from(values: T) -> Self;
}
///See custom from trait
impl<T: Clone + 'static> CustomFrom<T> for Dynamic {
    fn custom_from(value: T) -> Self {
        Dynamic(DynamicClone(Box::new(MyLangWrapper(Rc::new(value)))))
    }
}
///See custom from trait
pub trait LangInto<T> {
    fn lang_into(self) -> T;
}
///See custom from trait
impl<T, U: CustomFrom<T>> LangInto<U> for T //Copied from rust source
{
    fn lang_into(self) -> U {
        U::custom_from(self) //Call our custom from
    }
}

//The start of an implementation of a custom add for the language. I want it to implement the base add + more, but it is not fully implemented yet
/*
trait MyLangAdd<Rhs = Self>
{
    type Output;
    fn my_lang_add(self, rhs: Rhs) -> Self::Output;

}
impl<Out: Add<Right, Output = Out>, Right : Add<Out> > MyLangAdd for Out
 {
    type Output = Out;
}*/

///Standard library for language
pub mod lang_std {
    use std::fmt::Debug;
    ///A simple print trait that uses debug to allow easy printing of most types
    pub fn print<T>(item: T)
    where
        T: Debug + Clone,
    {
        println!("{:?}", item);
    }
}
