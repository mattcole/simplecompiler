#![allow(warnings)] //Auto generated code generates warnings. Haven't added anything to suppress them yet.
use macro_compiler::my_lang_macro;

my_lang_macro!({
    g = rust!(vec![1, 2, 3]);
    print(g);
    g.push(4);
    print(g);
    h = g; //This will clone
    h.push(5);
    print(h);
    print("g did not change due to clone on write semantics");
    print(g);
});
