use proc_macro::TokenStream;

use quote::{quote, quote_spanned, ToTokens};
use std::collections::HashSet;
use syn::{self, spanned::Spanned, *};

///A macro that generates rust code from my language
#[proc_macro]
pub fn my_lang_macro(input: TokenStream) -> TokenStream {
    let mut main: ItemFn = syn::parse(
        quote!(
            fn main() {}
        )
        .into(),
    )
    .unwrap();
    *main.block = parse_macro_input!(input as syn::Block);
    let main = parse_func(main);
    let main: TokenStream = main.to_token_stream().into();
    let header: TokenStream = quote!(
        use language_lib::lang_std::*;
    )
    .into();
    let file: TokenStream = header.into_iter().chain(main).collect();
    println!("{}", file);
    file
}
///Creates a compiler error macro and maps it over the span of the given element
fn create_error<T: syn::parse::Parse, U: Spanned>(span: U, message: &str) -> T {
    syn::parse(
        quote_spanned!(span.span()=>{compile_error!(#message);})
            .into_token_stream()
            .into(),
    )
    .unwrap_or_else(|_| panic!("Error in create error"))
}
///Parses a single upper level item from my language to Rust
fn parse_item(item: syn::Item) -> syn::Item {
    match item {
        Item::Fn(ItemFn {
            attrs,
            vis,
            sig,
            block,
        }) => Item::Fn(parse_func(ItemFn {
            attrs,
            vis,
            sig,
            block,
        })),
        _ => create_error(item, "Tried to use unimplemented upper level item"),
    }
}
///Parses a function to Rust. Scans for variable usage and hoists variable declarations to the top of the function
fn parse_func(mut item: ItemFn) -> ItemFn {
    let mut set: HashSet<syn::Ident> = HashSet::new();
    item.block = parse_block(item.block, &mut set);
    item.block.stmts = set
        .into_iter()
        .map(ident_to_stmt)
        .chain(item.block.stmts)
        .collect();
    item
}
///Creates a initialization statement for a variable based on its identifier
fn ident_to_stmt(ident: syn::Ident) -> syn::Stmt {
    let stream: TokenStream = quote!(let mut #ident : _;).into(); //We need .into to get from TokenStream2 to TokenStream
    syn::parse(stream).unwrap_or_else(|_| panic!("Error in declaration hoisting"))
}
///Parses an entire block
fn parse_block(mut block: Box<syn::Block>, set: &mut HashSet<syn::Ident>) -> Box<syn::Block> {
    block.stmts = block
        .stmts
        .into_iter()
        .map(|stmt| parse_stmt(stmt, set))
        .collect();
    block
}
///Parses a statement and stores any encountered variables in the provided hashset
fn parse_stmt(stmt: syn::Stmt, set: &mut HashSet<syn::Ident>) -> syn::Stmt {
    match stmt {
        syn::Stmt::Expr(expr, token) => syn::Stmt::Expr(parse_expr(expr, false, set), token),
        syn::Stmt::Macro(stmt_macro) => parse_stmt_macro(stmt_macro),
        syn::Stmt::Item(item) => syn::Stmt::Item(parse_item(item)),
        _ => create_error(stmt, "This stmt is not defined in MyLang"),
    }
}
///Parses a macro at the statement level. Effectively just checks to see if it is rust!() and otherwise returns an error block
fn parse_stmt_macro(mac: syn::StmtMacro) -> syn::Stmt {
    if mac
        .mac
        .path
        .is_ident(&syn::parse::<syn::Ident>(quote!(rust).into()).unwrap())
    {
        syn::parse(mac.mac.tokens.clone().into()).unwrap_or_else(|err| {
            create_error(
                mac,
                &("Error in rust macro: ".to_string() + &err.to_string()),
            )
        })
    } else {
        create_error(mac, "This macro is not defined in MyLang!")
    }
}
///Basically copy of parse stmt macro except for expressions
fn parse_expr_macro(mac: syn::ExprMacro) -> syn::Expr {
    if mac
        .mac
        .path
        .is_ident(&syn::parse::<syn::Ident>(quote!(rust).into()).unwrap())
    {
        let result = syn::parse::<syn::Expr>(mac.mac.tokens.clone().into());
        match result {
            Ok(internal) => internal,
            Err(err) => create_error(
                mac,
                &("Error in rust macro: ".to_string() + &err.to_string()),
            ),
        }
    } else {
        create_error(mac, "This macro is not defined in MyLang!")
    }
}
///Parses an expression into Rust. Returns a compiler error over the span of the element if the code to handle it hasn't been written
fn parse_expr(expr: syn::Expr, parse_into: bool, set: &mut HashSet<syn::Ident>) -> syn::Expr {
    let expr = match expr {
        syn::Expr::Assign(assign) => syn::Expr::Assign(parse_assign(assign, set)),
        syn::Expr::Lit(lit) => parse_literal(lit),
        syn::Expr::Call(expr_call) => syn::Expr::Call(parse_function_call(expr_call, set)),
        syn::Expr::Binary(binop) => syn::Expr::Binary(syn::ExprBinary {
            attrs: binop.attrs,
            left: Box::new(parse_expr(*binop.left, false, set)),
            op: binop.op,
            right: Box::new(parse_expr(*binop.right, false, set)),
        }),
        syn::Expr::Path(path) => clone_variable(path),
        syn::Expr::MethodCall(method_call) => {
            syn::Expr::MethodCall(parse_method_call(method_call, set))
        }
        syn::Expr::Reference(ExprReference {
            attrs,
            and_token,
            mutability,
            mut expr,
        }) => {
            *expr = parse_expr(*expr, false, set);
            syn::Expr::Reference(ExprReference {
                attrs,
                and_token,
                mutability,
                expr,
            })
        }
        syn::Expr::Macro(expr_macro) => parse_expr_macro(expr_macro),
        _ => create_error(
            expr.clone(),
            &format!("This expr is not defined in MyLang: {:?}", expr),
        ),
    };
    if parse_into {
        syn::parse(quote!(#expr.into()).into()).unwrap()
    } else {
        expr
    }
}
///Creates a small block of code that clones and returns a variable. This allows us to achieve the languages copy semantics. Ideally, we would want a more sophisitcated system using reference counting.
fn clone_variable(var: ExprPath) -> syn::Expr {
    let span = var.span();
    syn::parse(
        quote_spanned!(span.span()=>{#var.clone()})
            .into_token_stream()
            .into(),
    )
    .unwrap_or_else(|_| panic!("Error when adding clone to variable"))
}
///Parses a function call. Effectivly just call parse expr on every expression.
fn parse_function_call(mut call: syn::ExprCall, set: &mut HashSet<syn::Ident>) -> syn::ExprCall {
    call.args
        .iter_mut()
        .for_each(|param| *param = parse_expr(param.clone(), false, set));
    syn::ExprCall {
        attrs: call.attrs,
        func: call.func,
        paren_token: call.paren_token,
        args: call.args,
    }
}
///Nearly identical code to function_call except we are performing it with methods. Notably, we don't do any translation for the 'recieving' object.
fn parse_method_call(
    call: syn::ExprMethodCall,
    set: &mut HashSet<syn::Ident>,
) -> syn::ExprMethodCall {
    let syn::ExprMethodCall {
        attrs,
        receiver,
        dot_token,
        method,
        turbofish,
        paren_token,
        mut args,
    } = call;
    args.iter_mut()
        .for_each(|param| *param = parse_expr(param.clone(), false, set));
    syn::ExprMethodCall {
        attrs,
        receiver,
        dot_token,
        method,
        turbofish,
        paren_token,
        args,
    }
}
///Parses a literal. Notably, it adds a call to_string() on all string constants. Otherwise, it mostly just returns the literal unchanged.
fn parse_literal(expr_lit: ExprLit) -> syn::Expr {
    let span = expr_lit.span();
    let lit = expr_lit.lit.clone();
    match expr_lit.lit {
        syn::Lit::Str(_str) => syn::parse(quote_spanned!(span=>#lit.to_string()).into()).unwrap(),
        _ => syn::Expr::Lit(ExprLit {
            attrs: expr_lit.attrs,
            lit: expr_lit.lit,
        }),
    }
}
///Parses an assignment. This is where we actually get to add identifiers of variables we encounter to the hashset.
fn parse_assign(mut assign: syn::ExprAssign, set: &mut HashSet<syn::Ident>) -> syn::ExprAssign {
    if let syn::Expr::Path(expr_path) = assign.left.as_ref() {
        if let Some(ident) = expr_path.path.get_ident() {
            set.insert(ident.to_owned());
        }
    }
    assign.right = Box::new(parse_expr(*assign.right, false, set));
    assign
}
